#########################################################################
##                                                                     ##
##     Dockerfile                                                      ##
##     Bowtie2 extended                                                ##
##                                                                     ##
##     Gael A. Millot                                                  ##
##     Bioinformatics and Biostatistics Hub                            ##
##     Computational Biology Department                                ##
##     Institut Pasteur Paris                                          ##
##                                                                     ##
#########################################################################



# to get the log of the building:
# sudo docker run -ti --entrypoint bash <IMAGE_NAME>
# cat building.log

# to get all the ubuntu packages:
# sudo docker run -ti --entrypoint bash <IMAGE_NAME>
# apt-get update ; apt-cache policy zlib1g liblzma5 libncurses5 libtbb2 perl python 

# to export a file from the container:
# sudo docker container ls -a # to get all the active containers
# sudo docker cp <containerNAMES>:/building.log </host/path/target>

# samtools
# http://www.htslib.org/
# https://github.com/samtools/samtools/tags
# v1.14

# bowtie2
# http://bowtie-bio.sf.net/bowtie2
# v2.3.4.3



# base image: ubuntu:16.04
FROM gmillot/bash-extended_v3.0:gitlab_v4.0
# Warning: the name must be exactly what is written in the docker images interface, or sudo docker image ls

LABEL Gael.Millot=gael.millot@pasteur.fr
LABEL gitlab.dockerfiles="https://gitlab.pasteur.fr/gmillot/dockerfiles"


RUN echo "\n\n\n\n================\n\napt-get update\n\n================\n\n\n\n" > /building.log \
  && apt-get update --fix-missing \
    | tee -a /building.log \
    # samtools install
    && echo "\n\n\n\n================\n\napt-get install\n\n================\n\n\n\n" >> /building.log \
    && apt-get install -y \
        wget \
        gcc \
        make \
        libbz2-dev \
        zlib1g \
        zlib1g-dev \
        liblzma5 \
        liblzma-dev \
        libncurses5 \
        libncurses5-dev \
        bzip2 \
        | tee -a /building.log \
    && echo "\n\n\n\n================\n\nsamtools installation\n\n================\n\n\n\n" >> /building.log \
    && cd /usr/local/ \
    && wget -O samtools.tar.bz2 https://github.com/samtools/samtools/releases/download/1.14/samtools-1.14.tar.bz2 \
    && tar -xjvf samtools.tar.bz2 \
    && rm -rf samtools.tar.bz2 \
    && cd samtools-1.14 \
    && ./configure \
    && make \
    && make install \
    && cd /usr/local \
    && rm -rf /usr/local/samtools-1.14 \
    && apt-get remove -y \
        wget \
        gcc \
        make \
        libbz2-dev \
        zlib1g-dev \
        liblzma-dev \
        libncurses5-dev \
        bzip2 \
    # end samtools install
    # bowtie2 install
    && echo "\n\n\n\n================\n\napt-get install\n\n================\n\n\n\n" >> /building.log \
    && apt-get install -y \
        wget \
        gcc \
        g++ \
        make \
        libtbb2 \
        libtbb-dev \
        zlib1g \
        zlib1g-dev \
        perl \
        python \
    && echo "\n\n\n\n================\n\nbowtie2 installation\n\n================\n\n\n\n" >> /building.log \
    && cd /usr/local/ \
    && wget -O bowtie.tar.gz  https://github.com/BenLangmead/bowtie2/archive/v2.3.4.3.tar.gz \
    && tar -xzvf bowtie.tar.gz \
    && rm -rf bowtie.tar.gz \
    && cd bowtie2-2.3.4.3 \
    && make \
    && mv bowtie2 bowtie2-align-s bowtie2-align-l bowtie2-build \
       bowtie2-build-s bowtie2-build-l bowtie2-inspect \
       bowtie2-inspect-s bowtie2-inspect-l /usr/local/bin/ \
    && cd /usr/local \
    && rm -rf /usr/local/bowtie2-2.3.4.3 \
    && apt-get remove -y \
        wget \
        gcc \
        g++ \
        make \
        libtbb-dev \
        zlib1g-dev \
    # end bowtie2 install
    # cleaning
    && echo "\n\n\n\n================\n\napt-get autoremove\n\n================\n\n\n\n" >> /building.log \
    && apt-get autoremove -y \
    | tee -a /building.log ; echo "\n\n\n\n================\n\napt-get clean\n\n================\n\n\n\n" >> /building.log \
    && apt-get clean \
    | tee -a /building.log ; echo "\n\n\n\n================\n\nrm\n\n================\n\n\n\n" >> /building.log \
    && rm -rf /var/lib/apt/lists/* \
    | tee -a /building.log \
    && echo "\n\n\n\n================\n\ninstalled packages\n\n================\n\n\n\n" >> /building.log \
    && apt-cache policy zlib1g liblzma5 libncurses5 libtbb2 perl python >> /building.log

ENTRYPOINT ["/bin/bash"]
# ENTRYPOINT ["/usr/local/bin/samtools"]