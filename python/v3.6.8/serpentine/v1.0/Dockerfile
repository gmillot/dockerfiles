#########################################################################
##                                                                     ##
##     Dockerfile                                                      ##
##     Python Serpentine                                               ##
##                                                                     ##
##     Gael A. Millot                                                  ##
##     Bioinformatics and Biostatistics Hub                            ##
##     Computational Biology Department                                ##
##     Institut Pasteur Paris                                          ##
##                                                                     ##
#########################################################################


# to see the log of the building:
# sudo docker run -ti --entrypoint bash <IMAGE_NAME>
# cat building.log

# to export the log file of the image building from the container:
# sudo docker images
# sudo docker run -ti --entrypoint bash <IMAGE_NAME>
# exit
# sudo docker container ls -a # to get all the active containers
# sudo docker cp <containerNAMES>:/building.log </host/path/target>


FROM gmillot/python_v3.8.6_base_v1.0:gitlab_v8.6

LABEL Gael.Millot=gael.millot@pasteur.fr
LABEL gitlab.dockerfiles="https://gitlab.pasteur.fr/gmillot/dockerfiles"

ENV PY_LIB="\
  numpy \
  pandas \
  python-dateutil \
  serpentine \
"

    # install the packages
RUN echo "\n\n\n\n================\n\npip install\n\n================\n\n\n\n" > /building.log \
    && pip install $PY_LIB | tee -a /building.log \
    && echo "\n\n\n\n================\n\nPIP PACKAGES INSTALLED\n\n================\n\n\n\n" >> /building.log \
    && pip list >> /building.log \
    # end install the packages
    # inactivated packages because are in the base installation

ENV LC_ALL en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8
ENV LC_NUMERIC en_US.UTF-8

ENTRYPOINT ["/usr/bin/python3"]
