#########################################################################
##                                                                     ##
##     Dockerfile                                                      ##
##     R extended                                                      ##
##                                                                     ##
##     Gael A. Millot                                                  ##
##     Bioinformatics and Biostatistics Hub                            ##
##     Computational Biology Department                                ##
##     Institut Pasteur Paris                                          ##
##                                                                     ##
#########################################################################



# to see the log of the building:
# sudo docker run -ti --entrypoint bash <IMAGE_NAME>
# cat building.log

# to export the log file of the image building from the container:
# sudo docker images
# sudo docker run -ti --entrypoint bash <IMAGE_NAME>
# exit
# sudo docker container ls -a # to get all the active containers
# sudo docker cp <containerNAMES>:/building.log </host/path/target>



# base image: r-base:4.1.2
FROM gmillot/r-base_v4.1.2:gitlab_v8.2
# Warning: the name must be exactly what is written in the docker images interface, or sudo docker image ls. Here, the image is present locally, or distantly on the gmillot repo. Thus, gmillot/r-base:4.0.5 is correct.

LABEL Gael.Millot=gael.millot@pasteur.fr
LABEL gitlab.dockerfiles="https://gitlab.pasteur.fr/gmillot/dockerfiles"


ENV APT_GET_LINUX_LIB="\
    procps \
    libfontconfig1-dev \
"

ENV APT_LINUX_LIB="\
    build-essential \
    libcurl4-gnutls-dev \
    libxml2-dev \
    libssl-dev \
    libgit2-dev \
    libcairo2-dev \
    libxt-dev \
"

ENV CRANREPO="'https://cran.irsn.fr/'"

# Warning : add BiocManager and devtool at first positions and ggplot2 at last in the list below.
# SARTools package removed here because require a version of edgeR that I cannot find (R v4.0.5 not adapted for SARTool last version).
# translations package removed here because I could not find a way to install it without error, even with gettext ubuntu package.
ENV RLIB="\
  'BiocManager' \
  'devtools' \
  'abind' \
  'annotate' \
  'AnnotationDbi' \
  'askpass' \
  'assertthat' \
  'backports' \
  'base64enc' \
  'BH' \
  'Biobase' \
  'BiocGenerics' \
  'BiocParallel' \
  'BiocVersion' \
  'Biostrings' \
  'bit' \
  'bit64' \
  'bitops' \
  'blob' \
  'brew' \
  'brio' \
  'broom' \
  'cachem' \
  'Cairo' \
  'callr' \
  'car' \
  'carData' \
  'caTools' \
  'cellranger' \
  'CGHbase' \
  'CGHcall' \
  'cli' \
  'clipr' \
  'colorspace' \
  'commonmark' \
  'conquer' \
  'corrplot' \
  'covr' \
  'cpp11' \
  'crayon' \
  'credentials' \
  'crosstalk' \
  'curl' \
  'data.table' \
  'DBI' \
  'dbplyr' \
  'DelayedArray' \
  'desc' \
  'DESeq2' \
  'diffobj' \
  'digest' \
  'DNAcopy' \
  'dplyr' \
  'DT' \
  'dtplyr' \
  'edgeR' \
  'ellipsis' \
  'emmeans' \
  'estimability' \
  'evaluate' \
  'ExPosition' \
  'fansi' \
  'farver' \
  'fastmap' \
  'forcats' \
  'formatR' \
  'fs' \
  'futile.logger' \
  'futile.options' \
  'future' \
  'future.apply' \
  'gargle' \
  'genefilter' \
  'geneplotter' \
  'generics' \
  'GenomeInfoDb' \
  'GenomeInfoDbData' \
  'GenomicRanges' \
  'gert' \
  'GGally' \
  'ggdendro' \
  'ggrepel' \
  'ggseqlogo' \
  'gh' \
  'gitcreds' \
  'githubinstall' \
  'globals' \
  'glue' \
  'googledrive' \
  'googlesheets4' \
  'gplots' \
  'gridExtra' \
  'gtable' \
  'gtools' \
  'haven' \
  'HiCcompare' \
  'highr' \
  'hms' \
  'htmltools' \
  'htmlwidgets' \
  'httr' \
  'ids' \
  'impute' \
  'ini' \
  'InteractionSet' \
  'IRanges' \
  'isoband' \
  'jpeg' \
  'jsonlite' \
  'kableExtra' \
  'knitr' \
  'labeling' \
  'lambda.r' \
  'later' \
  'lazyeval' \
  'lemon' \
  'lifecycle' \
  'limma' \
  'listenv' \
  'lme4' \
  'lmerTest' \
  'locfit' \
  'lubridate' \
  'magrittr' \
  'maptools' \
  'markdown' \
  'marray' \
  'MatrixGenerics' \
  'MatrixModels' \
  'matrixStats' \
  'memoise' \
  'mime' \
  'minqa' \
  'mockery' \
  'modelr' \
  'munsell' \
  'mvtnorm' \
  'nloptr' \
  'numDeriv' \
  'openssl' \
  'openxlsx' \
  'parallelly' \
  'pbkrtest' \
  'pheatmap' \
  'pillar' \
  'pkgbuild' \
  'pkgconfig' \
  'pkgload' \
  'plogr' \
  'plyr' \
  'png' \
  'praise' \
  'prettyGraphs' \
  'prettyunits' \
  'processx' \
  'progress' \
  'promises' \
  'ps' \
  'pspearman' \
  'purrr' \
  'QDNAseq' \
  'quantreg' \
  'R.methodsS3' \
  'R.oo' \
  'R.utils' \
  'R6' \
  'rappdirs' \
  'rcmdcheck' \
  'RColorBrewer' \
  'Rcpp' \
  'RcppArmadillo' \
  'RcppEigen' \
  'RCurl' \
  'readr' \
  'readxl' \
  'rematch' \
  'rematch2' \
  'remotes' \
  'reprex' \
  'reshape' \
  'reshape2' \
  'reticulate' \
  'rex' \
  'rhdf5' \
  'rhdf5filters' \
  'Rhdf5lib' \
  'Rhtslib' \
  'rio' \
  'rlang' \
  'rmarkdown' \
  'roxygen2' \
  'rprojroot' \
  'Rsamtools' \
  'RSQLite' \
  'rstudioapi' \
  'rversions' \
  'rvest' \
  'S4Vectors' \
  'scales' \
  'selectr' \
  'sessioninfo' \
  'snow' \
  'snowfall' \
  'sp' \
  'SparseM' \
  'statmod' \
  'stringi' \
  'stringr' \
  'SummarizedExperiment' \
  'SuppDists' \
  'svglite' \
  'sys' \
  'systemfonts' \
  'testthat' \
  'tibble' \
  'tidyr' \
  'tidyselect' \
  'tidyverse' \
  'tinytex' \
  'usethis' \
  'utf8' \
  'uuid' \
  'vctrs' \
  'viridisLite' \
  'waldo' \
  'webshot' \
  'whisker' \
  'withr' \
  'xfun' \
  'XML' \
  'xml2' \
  'xopen' \
  'xtable' \
  'XVector' \
  'yaml' \
  'zip' \
  'zlibbioc' \
  'ggplot2' \
"

# Warning : add BiocManager and devtool at first positions and ggplot2 at last.


# nextflow needs procps for usage stats, cpu and process memory. Normally, this package is present by default in ubuntu. But in the R base image, it seems that there is a problem.
# libfontconfig1-dev is for blob.
# libcurl4-gnutls-dev libxml2-dev libssl-dev libgit2-dev required for devtool package installation.
# libcairo2-dev and libxt-dev required for Cairo package installation.
# fun.warning.capture from fun_get_message() of the cute package.
# I used a first loop because I cannot use array in dockerfiles.
RUN echo "\n\n\n\n================\n\napt-get update\n\n================\n\n\n\n" > building.log \
  && apt-get update \
    | tee -a building.log ; echo "\n\n\n\n================\n\napt-get install\n\n================\n\n\n\n" >> building.log \
  && apt-get install -y $APT_GET_LINUX_LIB \
    | tee -a building.log ; echo "\n\n\n\n================\n\napt install\n\n================\n\n\n\n" >> building.log \
  && apt -y install $APT_LINUX_LIB \
    | tee -a building.log \
  && RLIB_LENGTH=0 \
  && for i2 in $RLIB ; do RLIB_LENGTH=$((RLIB_LENGTH+1)) ; done \
  && COUNT=0 \
  && for i2 in $RLIB ; do \
    COUNT=$((COUNT+1)) ; \
    Rscript --vanilla -e " \
      options(warn = 1, verbose = TRUE) ; \
      tempo <- file('building.log', open = 'a+b') ; \
      sink(file = tempo, append = TRUE, type = 'message') ; \
      fun.warning.capture <- function(expr){ \
        W <- NULL ; \
        w.handler <- function(w){ \
          W <<- w ; \
          invokeRestart('muffleWarning') \
        } ; \
        output <- list( \
          value = suppressMessages(withCallingHandlers(tryCatch(expr, error = function(e){e}), warning = w.handler)),  \
          warning = W \
        ) ; \
        return(if(is.null(output\$warning)){NULL}else{as.character(output\$warning)}) \
      } ; \
      message(paste0('\n\n\n\n================\n\nR PACKAGE INSTALLATION ', $COUNT, ' / ', $RLIB_LENGTH, ': ', $i2, '\n\n================\n\n\n\n')) ; \
      if( ! ($i2 %in% c('ggplot2', 'SARTools'))){ \
        tempo.warn <- fun.warning.capture(exp = install.packages(pkgs = $i2)) ; \
        if(is.null(tempo.warn)){ \
          install.packages(pkgs = $i2, lib = '/usr/local/lib/R/site-library', dependencies = NA, verbose = TRUE, repo = $CRANREPO) \
          }else{ \
            BiocManager::install(pkgs = $i2, lib = '/usr/local/lib/R/site-library', dependencies = NA, verbose = TRUE, update = FALSE) \
          } \
      }else if($i2 == 'ggplot2'){ \
        devtools::install_version(package = 'ggplot2', version = '3.3.3', lib = '/usr/local/lib/R/site-library', dependencies = NA, verbose = TRUE, repos = $CRANREPO) \
      }else if($i2 == 'SARTools'){ \
        devtools::install_github('PF2-pasteur-fr/SARTools', build_opts='--no-resave-data', lib = '/usr/local/lib/R/site-library', dependencies = NA, verbose = TRUE, upgrade = "always") \
      } \
    " ; \
  done \
  && Rscript --vanilla -e  " \
    options(warn = 1, verbose = TRUE) ; \
    args <- commandArgs(trailingOnly = TRUE) ; \
    if(any(is.na(args))){ \
        stop(paste0('\n\n================\n\nERROR IN plot_insertion.R\nTHE args OBJECT HAS NA\n\n================\n\n'), call. = FALSE) \
    } ; \
    tempo.arg.names <- c( \
        'RLIB' \
    ) ; \
    if(length(args) != length(tempo.arg.names)){ \
        stop(paste0('\n\n================\n\nERROR IN plot_insertion.R\nTHE NUMBER OF ELEMENTS IN args (', length(args),') IS DIFFERENT FROM THE NUMBER OF ELEMENTS IN tempo.arg.names (', length(tempo.arg.names),')\nargs:', paste0(args, collapse = ','), '\ntempo.arg.names:', paste0(tempo.arg.names, collapse = ','), '\n\n================\n\n'), call. = FALSE) \
    } ; \
    for(i1 in 1:length(tempo.arg.names)){ \
        assign(tempo.arg.names[i1], args[i1]) \
    } ; \
    RLIB <- strsplit(RLIB, split = ',')[[1]] ; \
    RLIB <- RLIB[RLIB != ''] ; \
    if(file.exists('installed_r_packages.csv')){ \
      file.remove('installed_r_packages.csv') \
    } ; \
    if(file.exists('not_installed_r_packages.txt')){ \
      file.remove('not_installed_r_packages.txt') \
    } ; \
    tempo <- file('building.log', open = 'a+b') ; \
    sink(file = tempo, append = TRUE, type = 'message') ; \
    message(paste0('\n\n\n\n================\n\nR PACKAGES INSTALLED\n\n================\n\n\n\n')) ; \
    message('SEE ALSO THE installed_r_packages.csv TABLE IN THE ROOT OF THE CONTAINER\n\n') ; \
    capture.output(as.data.frame(installed.packages())[1:4], file = tempo, append = TRUE, type = 'output') ; \
    df <- as.data.frame(installed.packages()) ; \
    for (i2 in 1:length(df)){ \
      df[, i2] <- gsub(x = df[, i2], replacement = '', pattern = '\n', fixed = TRUE) \
    } ; \
    write.table(x = df, file = 'installed_r_packages.csv', row.names = FALSE, col.names = TRUE, append = FALSE, quote = FALSE, sep = ';') ; \
    message(paste0('\n\n\n\n================\n\nR PACKAGES NOT INSTALLED\n\n================\n\n\n\n')) ; \
    df2 <- as.data.frame(RLIB[ ! RLIB %in% df[ , 'Package']]) ; \
    if(nrow(df2) == 0){ \
      message('ALL THE PACKAGES HAVE BEEN INSTALLED\n\n') \
    }else{ \
      message('SEE ALSO THE not_installed_r_packages.txt TABLE IN THE ROOT OF THE CONTAINER\n\n') ; \
      names(df2) <- 'Name' ; \
      capture.output(df2, file = tempo, append = TRUE, type = 'output') ; \
      write.table(x = df2, file = 'not_installed_r_packages.txt', row.names = FALSE, col.names = TRUE, append = FALSE, quote = FALSE, sep = ';') ; \
    } \
  " $(echo "$RLIB" | sed 's/ \{1,\}/,/g' | sed "s/'//g") \
  && echo "\n\n\n\n================\n\napt-get autoremove\n\n================\n\n\n\n" >> building.log \
  && apt-get autoremove -y \
    | tee -a building.log ; echo "\n\n\n\n================\n\napt-get clean\n\n================\n\n\n\n" >> building.log \
  && apt-get clean \
    | tee -a building.log ; echo "\n\n\n\n================\n\nrm\n\n================\n\n\n\n" >> building.log \
  && rm -rf /var/lib/apt/lists/* \
    | tee -a building.log \
  && echo "\n\n\n\n================\n\nLINUX PACKAGES INSTALLED\n\n================\n\n\n\n" >> /building.log \
  && apt-cache policy $APT_GET_LINUX_LIB $APT_LINUX_LIB \
    >> /building.log

# dependencies=NA means c("Depends", "Imports", "LinkingTo"). Do no use TRUE -> install suggesting packages which inflates tremendously the image.

ENTRYPOINT ["/usr/bin/R"]
