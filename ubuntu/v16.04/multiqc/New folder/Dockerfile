#########################################################################
##                                                                     ##
##     Dockerfile                                                      ##
##     multiQC                                                         ##
##                                                                     ##
##     Gael A. Millot                                                  ##
##     Bioinformatics and Biostatistics Hub                            ##
##     Computational Biology Department                                ##
##     Institut Pasteur Paris                                          ##
##                                                                     ##
#########################################################################



# to see the log of the building:
# sudo docker run -ti --entrypoint bash <IMAGE_NAME>
# cat building.log

# to export the log file of the image building from the container:
# sudo docker images
# sudo docker run -ti --entrypoint bash <IMAGE_NAME>
# exit
# sudo docker container ls -a # to get all the active containers
# sudo docker cp <containerNAMES>:/building.log </host/path/target>

# Python
# https://www.gnu.org/software/coreutils/
# v3.8.2


# base image: ubuntu:16.04
FROM ubuntu:16.04
# Warning: the name must be exactly what is written in the docker images interface, or sudo docker image ls. But here, since the image is not present locally, docker will search it on docker hub. Thus, gmillot/ubuntu:16.04 iw not correct

LABEL Gael.Millot=gael.millot@pasteur.fr
LABEL gitlab.dockerfiles="https://gitlab.pasteur.fr/gmillot/dockerfiles"

# update apt-get
RUN echo "\n\n\n\n================\n\napt-get update\n\n================\n\n\n\n" > /building.log \
  && apt-get update --fix-missing \
    | tee -a /building.log \
    # install US locales
    && echo "\n\n\n\n================\n\napt-get install\n\n================\n\n\n\n" >> /building.log \
    && apt-get install -y \
        locales \
        locales-all \
        | tee -a /building.log \
    # end install US locales
    # install the packages
    && apt-get install -y \
    coreutils=8.25-2ubuntu3~16.04 \
    gawk=1:4.1.3+dfsg-0.1 \
    bc=1.06.95-9build1 \
    git=1:2.7.4-0ubuntu1.10 \
    | tee -a /building.log \
    # end install the packages
    # cleaning
    && echo "\n\n\n\n================\n\napt-get autoremove\n\n================\n\n\n\n" >> /building.log \
    && apt-get autoremove -y \
    | tee -a /building.log ; echo "\n\n\n\n================\n\napt-get clean\n\n================\n\n\n\n" >> /building.log \
    && apt-get clean \
    | tee -a /building.log ; echo "\n\n\n\n================\n\nrm\n\n================\n\n\n\n" >> /building.log \
    && rm -rf /var/lib/apt/lists/* \
    | tee -a /building.log \
    && echo "\n\n\n\n================\n\ninstalled packages\n\n================\n\n\n\n" >> /building.log \
    && apt-cache policy \
        locales \
        locales-all \
         >> /building.log
# https://askubuntu.com/questions/179955/var-lib-apt-lists-is-huge
# folder filled after running sudo apt-get update (or use the Refresh button in a package manager), a list of packages will get downloaded from the Ubuntu servers. These files are then stored in /var/lib/apt/lists/. You can safely remove the contents of that directory as it is recreated when you refresh the package lists. If you remove the files, but do not run apt-get update to fetch the lists, commands like apt-cache will fail to provide information (since the cache is empty)
# apt-cache policy print all the indicated package info
# end cleaning

ENV LC_ALL en_US.UTF-8
ENV LANG en_US.UTF-8 # http://bugs.python.org/issue19846 # > At the moment, setting "LANG=C" on a Linux system *fundamentally breaks Python 3*, and that's not OK.
ENV LANGUAGE en_US.UTF-8
ENV LC_NUMERIC en_US.UTF-8
ENV PATH /usr/local/bin:$PATH # ensure local python is preferred over distribution python


ENTRYPOINT ["/bin/bash"]