
<br /><br />
## TABLE OF CONTENTS

   - [AIM](#aim)
   - [VERSIONS](#versions)
   - [LICENCE](#licence)
   - [CITATION](#citation)
   - [CREDITS](#credits)
   - [ACKNOWLEDGEMENTS](#Acknowledgements)
   - [WHAT'S NEW IN](#what's-new-in)



<br /><br />
## AIM

Dockerfiles that generated the images in [dockerhub](https://hub.docker.com/u/gmillot).



<br /><br />
## VERSIONS

The different releases are tagged [here](https://gitlab.pasteur.fr/gmillot/dockerfiles/-/tags)



<br /><br />
## LICENCE

These scripts can be redistributed and/or modified under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
Distributed in the hope that it will be useful, but without any warranty; without even the implied warranty of merchandability or fitness for a particular purpose.
See the GNU General Public License for more details at https://www.gnu.org/licenses or in the Licence.txt attached file.

<br /><br />
## CITATION

None



<br /><br />
## CREDITS

- [Gael A. Millot](https://gitlab.pasteur.fr/gmillot), Bioinformatics and Biostatistics Hub, Institut Pasteur, Paris, France
- Eric Deveaud, HPC Core Facility, Institut Pasteur, Paris, France



<br /><br />
## ACKNOWLEDGEMENTS

The developers & maintainers of the mentioned softwares and packages, including:<br />
- [R](https://www.r-project.org/)
- [Docker](https://www.docker.com/)
- [Gitlab](https://about.gitlab.com/)
- [Bash](https://www.gnu.org/software/bash/)
- [Ubuntu](https://ubuntu.com/)

Special acknowledgement to:<br />
- [Frédéric Lemoine](https://github.com/fredericlemoine), Bioinformatics and Biostatistics Hub, Institut Pasteur, Paris, France
- [Bertrand Néron](https://github.com/bneron), Bioinformatics and Biostatistics Hub, Institut Pasteur, Paris, France
- [Yang Cao](http://cao.labshare.cn/clab/index.html), Sichuan University, Chengdu, China, for the release of [Abalign](http://cao.labshare.cn/abalign/).



<br /><br />
## WHAT'S NEW IN

Use the gitlab tag of the docker image (e.g., gitlab_v5.0) to get the description of the modifications below

### 10.9

Dockerfiles added: <br />
fedora/v33/immcantation/v2.0 at date 20250128: from immcantation/suite:4.5.0, with lubridate and svglite packages added


### 10.8

Dockerfiles added: <br />
ubuntu/v20.04/alohomora_merlin_allegro/v1.0 at date 20231212: from ubuntu/v20.04/alohomora_merlin/v2.1:gitlab_v9.5, with allegro soft and alohomora_bch_haplo_20231212 and allegro_20231212.pl script files added
Warning: see section 8.5 below


### 10.7

Dockerfiles added: <br />
ubuntu/v20.04/alohomora_merlin/v2.2 at date 20231211: from ubuntu/v20.04/alohomora_merlin/v2.1:gitlab_v9.5, with allegro added
Warning: see section 8.5 below

### 10.6

Dockerfiles modified: <br />
python_v3.9.10_extended_v3.1 at date 20231201: from gmillot/python_v3.9.10_extended_v3.1:gitlab_v8.7 with pigz gzip and tabix added<br />


### 10.5

Dockerfiles modified: <br />
ubuntu_v22.4_bcftools_v1.18 at date 20231201: from gmillot/ubuntu_v22.4_bcftools_v1.18:gitlab_v10.4 with pigz added<br />


### 10.4

Dockerfiles modified: <br />
ubuntu_v22.4_bcftools_v1.18 at date 20231201: from gmillot/ubuntu_v22.4_bcftools_v1.18:gitlab_v10.3 with gzip and tabix added<br />


### 10.3

Dockerfiles added: <br />
ubuntu_v22.4_bcftools_v1.18 at date 20231128 <br />


### 10.2

Dockerfiles added: <br />
ubuntu/v22.04/base at date 20231114 <br />
ubuntu/v22.04/extended/v1.0 at date 20231114: from ubuntu:22.04 with wget git bc gawk coreutils added<br />
ubuntu/v22.04/abalign/v1.0 at date 20231114 <br />
python/v3.6.12/base/v1.0 at date 20231114 <br />
python/v3.6.12/nanoForkSpeed_basecalling/v1.0 at date 20231114 <br />


### 10.1

Dockerfiles modified: <br />
fedora/v33/immcantation/v1.2 at date 20230407: from gmillot/immcantation_v1.1:gitlab_v10.0  with lubridate added<br />


### 10.0

Dockerfiles modified: <br />
fedora/v33/immcantation/v1.1 at date 20230407: from gmillot/immcantation_v1.0:gitlab_v9.0  with svglite added<br />


### 9.9

Dockerfiles modified: <br />
R/v4.1.2/extended/v3.3 at date 20230331: from gmillot/r_v4.1.2_extended_v3.2:gitlab_v9.8  with lemon added<br />
R/v4.1.2/ig_clustering/v1.3 at date 20230331: from gmillot/r_v4.1.2_ig_clustering_v1.2:gitlab_v9.8  with lemon added<br />


### 9.8

Dockerfiles modified: <br />
R/v4.1.2/extended/v3.2 at date 20230323: from gmillot/r_v4.1.2_extended_v3.1:gitlab_v9.6  with ggrepel added<br />
R/v4.1.2/ig_clustering/v1.2 at date 20230323: from gmillot/r_v4.1.2_ig_clustering_v1.1:gitlab_v9.7  with ggrepel added<br />


### 9.7

Dockerfiles modified: <br />
R/v4.1.2/ig_clustering/v1.1 at date 20230211: from gmillot/r_v4.1.2_ig_clustering_v1.0:gitlab_v9.3  with svglite added and debugged<br />


### 9.6

Dockerfiles added: <br />
R/v4.1.2/extended/v3.1 at date 20230211: from gmillot/r_v4.1.2_extended_v3.0:gitlab_v9.3  with svglite added<br />
R/v4.1.2/ig_clustering/v1.1 at date 20230211: from gmillot/r_v4.1.2_ig_clustering_v1.0:gitlab_v9.3  with svglite added<br />


### 9.5

Dockerfiles added: <br />
ubuntu/v20.04/alohomora_merlin/v2.1 at date 20230122: from ubuntu:20.04, idem gmillot/alohomora_merlin_v2.0:gitlab_v9.4 with problem of locales solved
Warning: see section 8.5 below


### 9.4

Dockerfiles added: <br />
ubuntu/v20.04/alohomora_merlin/v2.0 at date 20221231: from ubuntu:20.04, idem gmillot/alohomora_merlin_v1.0:gitlab_v8.5 with R added
Warning: see section 8.5 below


### 9.3

Dockerfiles added: <br />
R/v4.1.2/extended/v3.0 at date 20221108: from gmillot/r-base_v4.1.2:gitlab_v8.2 used as base for R/v4.1.2/ig_clustering
R/v4.1.2/ig_clustering/v1.0 at date 20221108: from gmillot/r_v4.1.2_extended_v3.0:gitlab_v9.3 <br />


### 9.2

Dockerfiles added: <br />
python/3.9.10/changeo_presto/v1.0 at date 20221007: from ubuntu:20.04 & python v3.9.10 <br />


### 9.1

Dockerfiles added: <br />
ubuntu/v20.04/extended/v1.0 at date 20221002: from ubuntu:20.04 with git bc gawk coreutils added<br />
ubuntu/v20.04/slivar/v2.0 at date 20221002: from ubuntu:20.04 with samtools and slivar added<br />
ubuntu/v20.04/samtools/v1.0 at date 20221002: from ubuntu:20.04 with samtools added<br />
ubuntu/v20.04/htslib/v1.0 at date 20221002: from ubuntu:20.04 with samtools added


### 9.0

Dockerfiles added: <br />
fedora/v33/immcantation/v1.0 at date 20220928: docker image of immancation


### 8.11

Dockerfiles added: <br />
ubuntu/v20.04/slivar/v1.0 at date 20220705: from ubuntu:20.04 with slivar added


### 8.10

Dockerfiles added: <br />
R/v4.1.2/extended/v1.1 at date 20220628: for slitherine, from gmillot/r-base_v4.1.2:gitlab_v8.2 with notably HiCcompare added


### v8.9

Dockerfiles added: <br />
R/v4.1.2/contracted/v1.0 at date 20220622: 


### v8.8

Dockerfiles added: <br />
R/v4.1.2/extended/v2.1 at date 20220607: from gmillot/r_v4.1.2_extended_v2.0:gitlab_v8.2 with qqman package added


### v8.7

Dockerfiles added: <br />
python/v3.9.10/extended/v3.1 at date 20220504: from gmillot/python_v3.9.10_extended_v2.0:gitlab_v8.4 with cyvcf2 and scipy packages added


### v8.6

Dockerfiles added: <br />
python/v3.6.8/base/v1.0 at date 20220504: FROM python:3.6.8<br />
python/v3.6.8/serpentine/v1.0 at date 20220504: from gmillot/python_v3.8.6_base_v1.0:gitlab_v8.6<br />
python/v3.9.10/extended/v3.0 at date 20220504: from gmillot/python_v3.9.10_extended_v2.0:gitlab_v8.4 with cyvcf2 package added


### v8.5

Dockerfiles added: <br />
ubuntu/v16.04/base at date 20220422 <br />
ubuntu/v20.04/base at date 20220422 <br />
ubuntu/v20.04/alohomora_merlin/v1.0 at date 20220422
<br />
Warning: do not use --entrypoint bash for image alohomora_merlin/v1.0, because modules will not be available.<br />
Just use : sudo docker run -ti d6e1595795cf<br />
<br /><br />
Explanation:<br />
ENTRYPOINT=["/usr/bin/bash" , "-l"]<br />
Pourquoi du coup cela marche avec /usr/bin/bash -l ?<br />
<br /><br />
Shell de login vs shell classique<br />
Shell de login source les fichiers d’init systemes<br />
Shell classique source les fichiers d’init user


### v8.4

Dockerfiles added: <br />
python/v3.9.10/base/v1.0 at date 20220318: using the debian 11 bullseye system. A python base in my repo in case the distant one is lost <br />
python/v3.9.10/extended/v1.1 at date 20220318: same as v1.0 but using the gmillot/python_v3.9.10_base_v1.0:gitlab_v8.4 image <br />
python/v3.9.10/extended/v2.0 at date 20220318: from gmillot/python_v3.9.10_extended_v1.1:gitlab_v8.4 with regex package added


### v8.3

Dockerfiles added: <br />
python/v3.9.10/extended/v1.0 at date 20220314: using the debian 11 bullseye system


### v8.2

Dockerfiles added: <br />
R/v4.1.2/base at date 20220310: using the ubuntu 20.04 system <br />
R/v4.1.2/extended v1.0 at date 20220310: base + 238 packages installed but 40 of them could not be properly installed due to a too much recent R version. Thus, plan a v1.1 later <br />
R/v4.1.2/extended v2.0 at date 20220310: v1.0 + 5 packages installed for rmarkdown and pdftools <br />
R/v4.0.5/extended v3.0 at date 20220308: v2.0 + pdftools but is not working because of the unstable debian system used


### v8.1

Dockerfiles modified: <br />
ubuntu/v16.04/alien_trimmer/v0.4.0 at date 20220114: locales added <br />


### v8.0

Dockerfiles added: <br />
ubuntu/v16.04/bedtools/v1.14 at date 20220114 <br />
Dockerfiles modified: <br />
ubuntu/v16.04/bash-extended/v4.0 at date 20220114: locales added <br />
ubuntu/v16.04/samtools/v1.14 at date 20220114: locales added <br />
ubuntu/v16.04/bowtie2-extended/v2.0 at date 20220114: locales added


### v7.0

Dockerfiles added: <br />
ubuntu/v16.04/bowtie2-extended/v1.0 at date 20220106 <br />
ubuntu/v16.04/samtools/v1.14 at date 20220106


### v6.4

Dockerfiles added: <br />
R/v4.0.5/extended v2.0 at date 20211228: v1.0 + pandoc added for knitting documents using R


### v6.3

Dockerfiles modified: <br />
R/v4.0.5/extended v1.0 at date 20211228: 1) log of the building execution + table of the installed packages available in the image, 2) version frozen for the ubuntu and R packages, 3) name of the image improved


### v6.2

Dockerfiles debugged: <br />
R/v4.0.5/extended at date 20211213: ggplot2 v3.3.3 to fit with cute and devtools correctly installed


### v6.1

Dockerfiles modified: <br />
R/v4.0.5/extended at date 20211212: dependencies=NA instead of dependencies=TRUE (no suggested packages installed)


### v6.0

Dockerfiles modified: <br />
R/v4.0.5/extended at date 20211211: location of the library folder forced


### v5.1

Dockerfiles modified: <br />
ubuntu/v16.04/alien_trimmer/v0.4.0 at date 20211117: debbugging (CR removed from AlienTrimmer.sh)


### v5.0

Dockerfiles modified: <br />
ubuntu/v16.04/alien_trimmer/v0.4.0 at date 20211117: backbone is older but lighter (bash-extended_v3.0:gitlab_v4.0 instead of bash-extended_v1.0:gitlab_v1.0)


### v4.0

Dockerfiles added: <br />
ubuntu/v16.04/bcftools/v1.14 at date 20211108 <br />
<br />
Dockerfiles modified: <br />
ubuntu/v16.04/bash-extended/v3.0 at date 20211108: installation made using apt-get install, instead of installing the source code. Warning, this v3.0 has older versions of added packages:

| Version | Packages | Size |
| --- | --- | --- |
| **V2.0** |<ul><li>coreutils/v8.30<br /></li><li>gawk/v5.1.0<br /></li><li>bc/v1.07<br /></li><li>git/v2.33.1<br /></li></li>| 1.3 GB |
| **V3.0** |<ul><li>coreutils/v8.25<br /></li><li>gawk/v4.1.3<br /></li><li>bc/v1.06.95<br /></li><li>git/v2.7.4<br /></li></li>| 81.7 MB |


### v3.0

Dockerfiles added: <br />
ubuntu/v16.04/git/v2.33.1 at date 20211029 <br />
ubuntu/v16.04/bash-extended/v2.0 at date 20211029 <br />
<br />
Dockerfiles modified: <br />
R/v4.0.5/base at date 20211002: comment added to explain the install of procps


### v2.0

Dockerfiles added: <br />
ubuntu/v16.04/alien_trimmer/v0.4.0 at date 20211001


### v1.0

Dockerfiles added: <br />
ubuntu/v16.04/coreutils/v8.30 at date 20211001<br />
ubuntu/v16.04/gawk/v5.1.0 at date 20211001<br />
ubuntu/v16.04/bc/v1.07 at date 20211001<br />
ubuntu/v16.04/bash-extended/v1.0 at date 20211001, which contains coreutils/v8.30, gawk/v5.1.0 and bc/v1.07

R/v4.0.5/base at date 20211001<br />
R/v4.0.5/extended at date 20211001



