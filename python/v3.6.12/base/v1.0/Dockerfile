#########################################################################
##                                                                     ##
##     Dockerfile                                                      ##
##     Python base                                                     ##
##                                                                     ##
##     Gael A. Millot                                                  ##
##     Bioinformatics and Biostatistics Hub                            ##
##     Computational Biology Department                                ##
##     Institut Pasteur Paris                                          ##
##                                                                     ##
#########################################################################


# to see the log of the building:
# sudo docker run -ti --entrypoint bash <IMAGE_NAME>
# cat building.log

# to export the log file of the image building from the container:
# sudo docker images
# sudo docker run -ti --entrypoint bash <IMAGE_NAME>
# exit
# sudo docker container ls -a # to get all the active containers
# sudo docker cp <containerNAMES>:/building.log </host/path/target>


FROM python:3.6.12

LABEL Gael.Millot=gael.millot@pasteur.fr
LABEL gitlab.dockerfiles="https://gitlab.pasteur.fr/gmillot/dockerfiles"

# update apt-get
RUN echo "\n\n\n\n================\n\napt-get update\n\n================\n\n\n\n" > /building.log \
    && apt-get update --fix-missing \
    | tee -a /building.log \
    # install the ubuntu packages
    && echo "\n\n\n\n================\n\napt-get install\n\n================\n\n\n\n" >> /building.log \
    && DEBIAN_FRONTEND="noninteractive" apt-get install -y \
        wget \
        build-essential \
        | tee -a /building.log


RUN apt-get install -y locales \
    | tee -a /building.log

# Set the locale
RUN sed -i '/en_US.UTF-8/s/^# //g' /etc/locale.gen \
    && locale-gen \
    | tee -a /building.log

ENV LC_ALL en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8
ENV LC_NUMERIC en_US.UTF-8

RUN echo "\n\n\n\n================\n\nPIP PACKAGES INSTALLED\n\n================\n\n\n\n" >> /building.log \
    && pip list >> /building.log
    # end install the packages
    # inactivated packages because are in the base installation

ENV LC_ALL en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8
ENV LC_NUMERIC en_US.UTF-8

ENTRYPOINT ["/usr/bin/python3"]
