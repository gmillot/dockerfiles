#########################################################################
##                                                                     ##
##     Dockerfile                                                      ##
##     fedora base                                                     ##
##                                                                     ##
##     Gael A. Millot                                                  ##
##     Bioinformatics and Biostatistics Hub                            ##
##     Institut Pasteur Paris                                          ##
##                                                                     ##
#########################################################################


# to see the log of the building:
# sudo docker run -ti --entrypoint bash <IMAGE_ID>
# cat building.log

# to export the log file of the image building from the container:
# sudo docker images
# sudo docker run -ti --entrypoint bash <IMAGE_ID>
# exit
# sudo docker container ls -a # to get all the active containers
# sudo docker cp <containerNAMES>:/building.log </host/path/target>
# sudo docker cp <containerNAMES>:/installed_r_packages.csv </host/path/target>

# see https://immcantation.readthedocs.io/en/stable/docker/intro.html
FROM immcantation/suite:4.5.0


LABEL Gael.Millot=gael.millot@pasteur.fr
LABEL gitlab.dockerfiles="https://gitlab.pasteur.fr/gmillot/dockerfiles"

LABEL Gael.Millot=gael.millot@pasteur.fr
LABEL gitlab.dockerfiles="https://gitlab.pasteur.fr/gmillot/dockerfiles"



ENV CRANREPO="'https://cran.irsn.fr/'"

ENV BIOCONDUCTOR_REPO="'https://cloud.r-project.org'"

ENV RLIB="\
  'lubridate' \
  'svglite' \
"


# fun.warning.capture from fun_get_message() of the cute package.
# I used a first loop because I cannot use array in dockerfiles.
# Inactivate sink(file = tempo, append = TRUE, type = 'message') for debbuging.
RUN echo "" > building.log \
  && RLIB_LENGTH=0 \
  && for i2 in $RLIB ; do RLIB_LENGTH=$((RLIB_LENGTH+1)) ; done \
  && COUNT=0 \
  && for i2 in $RLIB ; do \
    COUNT=$((COUNT+1)) ; \
    Rscript --vanilla -e " \
      options(warn = 1, verbose = TRUE) ; \
      tempo <- file('building.log', open = 'a+b') ; \
      sink(file = tempo, append = TRUE, type = 'message') ; \
      fun.warning.capture <- function(expr){ \
        W <- NULL ; \
        w.handler <- function(w){ \
          W <<- w ; \
          invokeRestart('muffleWarning') \
        } ; \
        output <- list( \
          value = suppressMessages(withCallingHandlers(tryCatch(expr, error = function(e){e}), warning = w.handler)),  \
          warning = W \
        ) ; \
        return(if(is.null(output\$warning)){NULL}else{as.character(output\$warning)}) \
      } ; \
      message(paste0('\n\n\n\n================\n\nR PACKAGE INSTALLATION ', $COUNT, ' / ', $RLIB_LENGTH, ': ', $i2, '\n\n================\n\n\n\n')) ; \
      if( ! ($i2 %in% c('ggplot2', 'SARTools'))){ \
        tempo.warn <- fun.warning.capture(exp = install.packages(pkgs = $i2,  dependencies = NA, verbose = TRUE, repo = $CRANREPO)) ; \
        if(is.null(tempo.warn)){ \
            tempo <- paste0('STANDARD INSTALLATION ATTEMPT FOR ', $i2) ; \
            message(paste0('\n\n', tempo, '\n\n')) ; \
            cat(paste0('\n\n\n', tempo, '\n\n\n')) ; \
            install.packages(pkgs = $i2,  dependencies = NA, verbose = TRUE, repo = $CRANREPO) \
        }else{ \
            tempo <- paste0('BiocManager INSTALLATION ATTEMPT FOR ', $i2) ; \
            message(paste0('\n\n', tempo, '\n\n')) ; \
            cat(paste0('\n\n\n', tempo, '\n\n\n')) ; \
            BiocManager::install(pkgs = $i2,  dependencies = NA, verbose = TRUE, update = FALSE, site_repository = $BIOCONDUCTOR_REPO) \
        } \
      }else if($i2 == 'ggplot2'){ \
        devtools::install_version(package = 'ggplot2', version = '3.3.3',  dependencies = NA, verbose = TRUE, repos = $CRANREPO) \
      }else if($i2 == 'SARTools'){ \
        devtools::install_github('PF2-pasteur-fr/SARTools', build_opts='--no-resave-data',  dependencies = NA, verbose = TRUE, upgrade = "always") \
      } \
    " | tee -a building.log ; \
  done \
  && Rscript --vanilla -e  " \
    options(warn = 1, verbose = TRUE) ; \
    args <- commandArgs(trailingOnly = TRUE) ; \
    if(any(is.na(args))){ \
        stop(paste0('\n\n================\n\nERROR IN plot_insertion.R\nTHE args OBJECT HAS NA\n\n================\n\n'), call. = FALSE) \
    } ; \
    tempo.arg.names <- c( \
        'RLIB' \
    ) ; \
    if(length(args) != length(tempo.arg.names)){ \
        stop(paste0('\n\n================\n\nERROR IN plot_insertion.R\nTHE NUMBER OF ELEMENTS IN args (', length(args),') IS DIFFERENT FROM THE NUMBER OF ELEMENTS IN tempo.arg.names (', length(tempo.arg.names),')\nargs:', paste0(args, collapse = ','), '\ntempo.arg.names:', paste0(tempo.arg.names, collapse = ','), '\n\n================\n\n'), call. = FALSE) \
    } ; \
    for(i1 in 1:length(tempo.arg.names)){ \
        assign(tempo.arg.names[i1], args[i1]) \
    } ; \
    RLIB <- strsplit(RLIB, split = ',')[[1]] ; \
    RLIB <- RLIB[RLIB != ''] ; \
    if(file.exists('installed_r_packages.csv')){ \
      file.remove('installed_r_packages.csv') \
    } ; \
    if(file.exists('not_installed_r_packages.txt')){ \
      file.remove('not_installed_r_packages.txt') \
    } ; \
    tempo <- file('building.log', open = 'a+b') ; \
    sink(file = tempo, append = TRUE, type = 'message') ; \
    message(paste0('\n\n\n\n================\n\nR PACKAGES INSTALLED\n\n================\n\n\n\n')) ; \
    message('SEE ALSO THE installed_r_packages.csv TABLE IN THE ROOT OF THE CONTAINER\n\n') ; \
    capture.output(as.data.frame(installed.packages())[1:4], file = tempo, append = TRUE, type = 'output') ; \
    df <- as.data.frame(installed.packages()) ; \
    for (i2 in 1:length(df)){ \
      df[, i2] <- gsub(x = df[, i2], replacement = '', pattern = '\n', fixed = TRUE) \
    } ; \
    write.table(x = df, file = 'installed_r_packages.csv', row.names = FALSE, col.names = TRUE, append = FALSE, quote = FALSE, sep = ';') ; \
    message(paste0('\n\n\n\n================\n\nR PACKAGES NOT INSTALLED\n\n================\n\n\n\n')) ; \
    df2 <- as.data.frame(RLIB[ ! RLIB %in% df[ , 'Package']]) ; \
    if(nrow(df2) == 0){ \
      message('ALL THE PACKAGES HAVE BEEN INSTALLED\n\n') \
    }else{ \
      message('SEE ALSO THE not_installed_r_packages.txt TABLE IN THE ROOT OF THE CONTAINER\n\n') ; \
      names(df2) <- 'Name' ; \
      capture.output(df2, file = tempo, append = TRUE, type = 'output') ; \
      write.table(x = df2, file = 'not_installed_r_packages.txt', row.names = FALSE, col.names = TRUE, append = FALSE, quote = FALSE, sep = ';') ; \
    } \
  " $(echo "$RLIB" | sed 's/ \{1,\}/,/g' | sed "s/'//g") | tee -a building.log

# dependencies=NA means c("Depends", "Imports", "LinkingTo"). Do no use TRUE -> install suggesting packages which inflates tremendously the image.

ENTRYPOINT ["/bin/bash"]



