#########################################################################
##                                                                     ##
##     Dockerfile                                                      ##
##     Slivar                                                          ##
##                                                                     ##
##     Gael A. Millot                                                  ##
##     Bioinformatics and Biostatistics Hub                            ##
##     Computational Biology Department                                ##
##     Institut Pasteur Paris                                          ##
##                                                                     ##
#########################################################################



# to see the log of the building:
# sudo docker run -ti --entrypoint bash <IMAGE_NAME>
# cat building.log

# to export the log file of the image building from the container:
# sudo docker images
# sudo docker run -ti --entrypoint bash <IMAGE_NAME>
# exit
# sudo docker container ls -a # to get all the active containers
# sudo docker cp <containerNAMES>:/building.log </host/path/target>


# base image: ubuntu:20.04
FROM ubuntu:20.04
# Warning: the name must be exactly what is written in the docker images interface, or sudo docker image ls. But here, since the image is not present locally, docker will search it on docker hub. Thus, gmillot/ubuntu:16.04 iw not correct

LABEL Gael.Millot=gael.millot@pasteur.fr
LABEL gitlab.dockerfiles="https://gitlab.pasteur.fr/gmillot/dockerfiles"


# add gensoft deb repo pgp public key to apt trusted ones
# and install modules
WORKDIR /usr/bin/
# to avoid cd. All the following commands will occurs here. In addition, opening a container will be at this node.


# update apt-get
RUN echo "\n\n\n\n================\n\napt-get update\n\n================\n\n\n\n" > /building.log \
    && apt-get update --fix-missing \
    | tee -a /building.log \
    # install the ubuntu packages
    && echo "\n\n\n\n================\n\napt-get install\n\n================\n\n\n\n" >> /building.log \
    && DEBIAN_FRONTEND="noninteractive" apt-get install -y \
        wget \
        build-essential \
        | tee -a /building.log


RUN echo "\n\n\n\n================\n\slivar install\n\n================\n\n\n\n" >> /building.log \
    && wget https://github.com/brentp/slivar/releases/download/v0.2.7/slivar \
    && chmod 755 slivar \
    | tee -a /building.log \

    # cleaning
RUN echo "\n\n\n\n================\n\napt-get autoremove\n\n================\n\n\n\n" >> /building.log \
    && apt-get autoremove -y \
    | tee -a /building.log ; echo "\n\n\n\n================\n\napt-get clean\n\n================\n\n\n\n" >> /building.log \
    && apt-get clean \
    | tee -a /building.log ; echo "\n\n\n\n================\n\nrm\n\n================\n\n\n\n" >> /building.log \
    && rm -rf /var/lib/apt/lists/* \
    | tee -a /building.log \
    && echo "\n\n\n\n================\n\ninstalled packages\n\n================\n\n\n\n" >> /building.log \
    && apt-cache policy \
        locales \
        locales-all \
         >> /building.log

ENV LC_ALL en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8
ENV LC_NUMERIC en_US.UTF-8

ENTRYPOINT ["/usr/bin/bash" , "-l"]
# The -l option (according to the man page) makes "bash act as if it had been invoked as a login shell". Login shells read certain initialization files from your home directory, such as .bash_profile. Variables set in .bash_profile override when bash launches.