#########################################################################
##                                                                     ##
##     Dockerfile                                                      ##
##     nanoForkSpeed_basecalling                                       ##
##                                                                     ##
##     Gael A. Millot                                                  ##
##     Bioinformatics and Biostatistics Hub                            ##
##     Computational Biology Department                                ##
##     Institut Pasteur Paris                                          ##
##                                                                     ##
#########################################################################



# to see the log of the building:
# sudo docker run -ti --entrypoint bash <IMAGE_NAME>
# cat building.log

# to export the log file of the image building from the container:
# sudo docker images
# sudo docker run -ti --entrypoint bash <IMAGE_NAME>
# exit
# sudo docker container ls -a # to get all the active containers
# sudo docker cp <containerNAMES>:/building.log </host/path/target>


# base image: ubuntu:20.04 and python v3.6.12
FROM gmillot/python_v3.6.12_base_v1.0:gitlab_v10.2
# Warning: the name must be exactly what is written in the docker images interface, or sudo docker image ls. But here, since the image is not present locally, docker will search it on docker hub. Thus, gmillot/ubuntu:16.04 iw not correct

LABEL Gael.Millot=gael.millot@pasteur.fr
LABEL gitlab.dockerfiles="https://gitlab.pasteur.fr/gmillot/dockerfiles"

ENV PY_LIB_PRE="\
    numpy==1.19.2\n \
"

ENV PY_LIB="\
    cached-property==1.5.2\n \
    cycler==0.10.0\n \
    h5py==3.1.0\n \
    joblib==1.0.0\n \
    kiwisolver==1.3.1\n \
    mappy==2.17\n \
    matplotlib==3.3.3\n \
    megalodon==2.2.9\n \
    ont-fast5-api==3.1.6\n \
    ont-pyguppy-client-lib==4.4.1\n \
    packaging==20.8\n \
    pandas==1.1.5\n \
    pillow==8.1.0\n \
    progressbar33==2.4\n \
    pyparsing==2.4.7\n \
    pysam==0.16.0.1\n \
    python-dateutil==2.8.1\n \
    pytz==2020.5\n \
    scikit-learn==0.24.0\n \
    scipy==1.5.4\n \
    seaborn==0.11.1\n \
    sklearn==0.0\n \
    threadpoolctl==2.1.0\n \
    tqdm==4.56.0 \
"

# add gensoft deb repo pgp public key to apt trusted ones
# and install modules
WORKDIR /usr/bin/
# to avoid cd. All the following commands will occurs here. In addition, opening a container will be at this node.

RUN echo "\n\n\n\n================\n\npre pip install\n\n================\n\n\n\n" >> /building.log \
    && echo "$PY_LIB_PRE" > caca.txt \
    && pip install -r caca.txt >> /building.log \
    && rm caca.txt

RUN echo "\n\n\n\n================\n\npip install\n\n================\n\n\n\n" >> /building.log \
    && echo "$PY_LIB" > caca.txt \
    && pip install -v -r caca.txt >> /building.log \
    && rm caca.txt

RUN echo "\n\n\n\n================\n\nubuntu installed packages\n\n================\n\n\n\n" >> /building.log \
    && apt-cache policy \
        locales \
        locales-all \
        $APT_GET_LINUX_LIB \
         >> /building.log


RUN echo "\n\n\n\n================\n\npip installed packages\n\n================\n\n\n\n" >> /building.log \
    && pip list >> /building.log
    # end install the packages
    # inactivated packages because are in the base installation

ENTRYPOINT ["/usr/bin/bash" , "-l"]
# The -l option (according to the man page) makes "bash act as if it had been invoked as a login shell". Login shells read certain initialization files from your home directory, such as .bash_profile. Variables set in .bash_profile override when bash launches.